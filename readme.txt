tisndrcv - Application/library for sending/receiving and manipulating TI files

This is a Python command-line script and module for sending and receiving all sorts of TI graphing calculator files over direct USB, as well as manipulating/converting between various PC formats.

It also (for now) contains an implementation of the TI-82 Advanced hacked backup exploit for native code execution.

>Setup
1. Install Python v3.5 or later.
If prompted during installation, choose to add it to your PATH environment variable.

2. Make sure libusb is installed. On Windows, install LibUSB-Win32.

3. Install PyUSB.
On Windows, this is done via command prompt with administrator privileges.

4. On Windows, you may need to install a filter driver for the calculator. If so, you may need to re-install this driver every time you plug the calculator into a different USB port.
To do this, you can run the filter wizard in LibUSB-Win32, or you can install all class filters (not recommended), or you can use a libusb driver, such as the one that comes with TiLP II (although this may also require installing a filter driver).

>Using tisndrcv
Just run it with -h or --help. You can also specify it with a command to get specific help for that command.

>Running the TI-82 Advanced Exploit
To run the exploit:
	1. Build the payload by running build.bat, which will create z.8xp.
	2. Embed the payload into the TI-82 Advanced backup file by running build_82a_backup.py, which will create 82A_exploit.82B.
	3. Send the TI-82 Advanced backup file by running: send_backup.py 82A_exploit.82B

>Customizing the TI-82 Advanced Exploit
Modify z.z80 as desired, then follow the "Running" instructions above.

>Help?
E-mail brandonlw@gmail.com for questions, comments, etc.
